using Authentication;
using CommonComponent.Email;
using CrudOperations;
using Microsoft.AspNetCore.DataProtection;
using System.Globalization;
using System.Text;
using log4net.Config;
using BoilerplateCleanArchitectureV3.Infrastructure;
using BoilerplateCleanArchitectureV3.Application.Interfaces.Account;
using BoilerplateCleanArchitectureV3.Application.Interfaces.ManageUsers;
using Microsoft.AspNetCore.Authentication;
using BoilerplateCleanArchitectureV3.Application.Services;
using BoilerplateCleanArchitectureV3.APIServices.CustomAuthenticationHandler;
using BoilerplateCleanArchitectureV3.APIServices.Filter;
using BoilerplateCleanArchitectureV3.APIServices.ActivityLogger;
using BoilerplateCleanArchitectureV3.APIServices.ErrorLogger;

var builder = WebApplication.CreateBuilder(args);
//var connectionString = builder.Configuration.GetConnectionString("AnjaliPMSDB") ?? throw new InvalidOperationException("Connection string not found.");
var connectionString = builder.Configuration.GetConnectionString("TestDb") ?? throw new InvalidOperationException("Connection string not found.");

log4net.Config.XmlConfigurator.Configure();
XmlConfigurator.Configure(new FileInfo("log4net.config"));
builder.Logging.AddLog4Net("log4net.config");

// Add services to the container.

builder.Services.AddControllers();
//builder.Services.AddControllers()
//    .AddNewtonsoftJson(options =>
//    {
//        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
//    });
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

var key = Encoding.ASCII.GetBytes(builder?.Configuration["SecretKey"]!);

// JWT Bearer Token Authentication
//builder!.Services.AddAuthentication(x =>
//{
//    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
//    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
//})
//            .AddJwtBearer(x =>
//            {
//                x.Events = new JwtBearerEvents
//                {
//                    OnTokenValidated = context =>
//                    {

//                        var userService = context.HttpContext.RequestServices.GetRequiredService<ICrudOperationService>();
//                        var userId = context.Principal!.Identity!.Name!;
//                        var user = userService.GetSingleRecord<UserBasicDetailsResponse>("[Account].[uspUserBasicDetailsGet]",
//                            new
//                            {
//                                userId
//                            }).GetAwaiter().GetResult();
//                        if (user == null)
//                        {
//                            // return unauthorized if user no longer exists
//                            context.Fail("Unauthorized");
//                        }
//                        return Task.CompletedTask;
//                    }
//                };
//                x.RequireHttpsMetadata = false;
//                x.SaveToken = true;
//                x.TokenValidationParameters = new TokenValidationParameters
//                {
//                    ValidateIssuerSigningKey = true,
//                    IssuerSigningKey = new SymmetricSecurityKey(key),
//                    ValidateIssuer = false,
//                    ValidateAudience = false,
//                    ClockSkew = TimeSpan.Zero
//                };
//            });

// Paseto Token Authentication

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = "Paseto";
    options.DefaultChallengeScheme = "Paseto";
})
.AddScheme<AuthenticationSchemeOptions, CustomAuthenticationHandler>("Paseto", (options) =>
{
    options.ClaimsIssuer = "";
});

builder.Services.AddSwaggerGen();

//builder.Services.AddSingleton<IAuthService>(x => new JwtTokenManager
//    (
//        x.GetService<IConfiguration>()!,
//        x.GetService<IDataProtectionProvider>()!,
//        connectionString 
//    ));

// Cors policy Added

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowSpecificOrigin",
        policyBuilder => policyBuilder.AllowAnyOrigin()
            // Specify the client app's origin
            .AllowAnyMethod()
            .AllowAnyHeader());
    //.AllowCredentials()); // Enable credentials
});

builder.Services.AddSingleton<IAuthService>(x => new PasetoTokenManager
  (
      x.GetService<IConfiguration>()!,
      x.GetService<IDataProtectionProvider>()!,
      connectionString));
builder.Services.AddScoped<ICrudOperationService>(x => new CrudOperationDataAccess(x.GetService<IConfiguration>()!, connectionString));

builder.Services.AddScoped<IEmailService>(x => new EmailService(new EmailConfig
{
    /*
     * Uncomment below line of code to add procedure for fetching content from database
     * 
     * EmailContentSpName = "[Account].[uspEmailTemplateContentList]", 
     *  
    */
    Username = (builder?.Configuration["AppSettings:Smtp:UserName"] ?? ""),
    Password = (builder?.Configuration["AppSettings:Smtp:Password"] ?? ""),
    EnableSSL = Convert.ToBoolean(builder?.Configuration["AppSettings:Smtp:EnableSSl"], CultureInfo.CurrentCulture),
    FromEmail = (builder?.Configuration["AppSettings:Smtp:From"] ?? ""),
    Host = (builder?.Configuration["AppSettings:Smtp:Host"] ?? ""),
    Port = Convert.ToInt32(builder?.Configuration["AppSettings:Smtp:Port"], CultureInfo.CurrentCulture)
}, connectionString));

builder.Services.AddScoped<IAccountService, AccountService>();
builder.Services.AddScoped<IAccountRepository, AccountRepository>();
builder.Services.AddScoped<IUserService , UserService>();
builder.Services.AddScoped<IUserRepository , UserRepository>();

builder.Services.AddScoped<ValidationFilter>();
builder.Services.AddScoped<ActivityLogFilterAttribute>();


//builder.Services.AddScoped<IAuthService, PasetoTokenManager>();
//builder.Services.AddScoped<CustomAuthenticationHandler>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware(typeof(ExceptionHandlingMiddleware));

app.UseHttpsRedirection();
app.UseAuthentication();

app.UseCors("AllowSpecificOrigin");

app.UseAuthorization();

app.MapControllers();

app.Run();




//var builder = WebApplication.CreateBuilder(args);

//// Add services to the container.

//builder.Services.AddControllers();
//// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
//builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();

//var app = builder.Build();

//// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

//app.UseHttpsRedirection();

//app.UseAuthorization();

//app.MapControllers();

//app.Run();
