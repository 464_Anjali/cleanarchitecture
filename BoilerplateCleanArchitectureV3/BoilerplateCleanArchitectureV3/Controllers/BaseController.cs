﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BoilerplateCleanArchitectureV3.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class BaseController : ControllerBase
    {
        protected void SetTokenInCookie(string encryptedToken)
        {
            Response.Cookies.Append("AuthToken", encryptedToken, new CookieOptions
            {
                HttpOnly = true,
                Secure = true,
                SameSite = SameSiteMode.Strict,
                IsEssential = true,
                Expires = DateTime.UtcNow.AddDays(1)
            });
        }

        protected string GetUserId()
        {
            return User.Identity!.Name!;
        }

        protected void DeleteTokenFromCookie()
        {
            Response.Cookies.Delete("AuthToken", new CookieOptions
            {
                HttpOnly = true,
                Secure = true,
                SameSite = SameSiteMode.Strict,
                IsEssential = true,
                // Expires = DateTime.UtcNow.AddDays(1)
            });
        }
    }
}

