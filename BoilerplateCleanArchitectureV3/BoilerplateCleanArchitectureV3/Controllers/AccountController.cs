﻿using Authentication;
using BoilerplateCleanArchitectureV3.Application;
using BoilerplateCleanArchitectureV3.Application.Interfaces.Account;
using BoilerplateCleanArchitectureV3.Domain.DtoModels;
using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using BoilerplateCleanArchitectureV3.Infrastructure;
using CrudOperations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Globalization;

namespace BoilerplateCleanArchitectureV3.Controllers
{
    [Route("api/account")]
   // [ApiController]
    public class AccountController : BaseController
    {

        private readonly IAccountService  _accountService;
        private readonly ICrudOperationService _crudOperations;
        private readonly IAuthService _authService;
        public AccountController(
                                    IAccountService accountService,
                                    ICrudOperationService crudOperations, 
                                    IAuthService authService
                                ) 
        {
            _accountService = accountService;
            _crudOperations = crudOperations;
            _authService = authService;
        }

        [HttpPost, Route("addusers")]
        public async Task<IActionResult> CreateAccount([FromBody] AccountCreationRequest dtoAccountCreation )
        {
            var response = await _accountService.AccountCreation(dtoAccountCreation).ConfigureAwait(false);

            return response!.Status ? StatusCode(StatusCodes.Status201Created, response) :
            (!response.Status ? StatusCode(StatusCodes.Status409Conflict, response) :
            StatusCode(StatusCodes.Status400BadRequest, "Something went wrong, Please contact system administrator"));
        }

        [HttpPost, Route("login")]
        public async Task<ActionResult> Login([FromBody] DtoLogin login)
        {
            var response = await _accountService.Login(login).ConfigureAwait(false);
            if (response.Status)
            {
                if (!string.IsNullOrWhiteSpace(response.Token))
                {
                    SetTokenInCookie(response.Token);
                }
                return Ok(new { response.Status, response.Message });
            }
            var errorMessageCode = response.Message?.Trim().ToLower(CultureInfo.CurrentCulture) ?? string.Empty;
            response.Message = Utilty.GetDataFromXML("ErrorMessage.xml", "error", errorMessageCode);

            var statusCode = errorMessageCode switch
            {
                "dau" or "deu" => StatusCodes.Status403Forbidden,
                "unf" or "invalid-credential" or "acl" => StatusCodes.Status404NotFound,
                _ => StatusCodes.Status400BadRequest
            };

            if (statusCode == StatusCodes.Status400BadRequest)
            {
                response.Message = "Something went wrong, please contact the system administrator.";
            }

            return StatusCode(statusCode, response);
        }

        [HttpPost, Route("password/forgot")]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequest forgotPasswordRequest )
        {
            var forgotPasswordResponse = await _accountService.ForgotPassword(forgotPasswordRequest).ConfigureAwait(false);

            return StatusCode(
                (!forgotPasswordResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK),
                new
                {
                    forgotPasswordResponse.Status,
                    forgotPasswordResponse.Message,
                });
        }


        [HttpGet, Route("password/verifytoken")]
        public async Task<IActionResult> ForgotPasswordVerifyToken([FromQuery] PasswordVerifyTokenRequest passwordVerifyTokenRequest)
        {
            var forgotPasswordResponse = await _accountService.VerifyPasswordToken(passwordVerifyTokenRequest).ConfigureAwait(false);
            return StatusCode(
                (!forgotPasswordResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK),
                new
                {
                    forgotPasswordResponse.Status,
                    forgotPasswordResponse.Message
                    //forgotPasswordResponse.Email,
                    //forgotPasswordResponse.EmailToken
                });
        }


        [HttpPost, Route("password/reset")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordRequest resetPassword)
        {
            var resetPasswordResponse = await _accountService.ResetPassword(resetPassword).ConfigureAwait(false);

            return StatusCode(
                (!resetPasswordResponse.Status ? StatusCodes.Status409Conflict : StatusCodes.Status200OK),
                new
                {
                    resetPasswordResponse.Status,
                    resetPasswordResponse.Message,
                });
        }

        [AllowAnonymous, HttpPost, Route("otp/resend")]
        public async Task<IActionResult> ResendOtp([FromBody] ResendOtpRequest resendOtp)
        {
            var resendOtpResponse = await _accountService.ResendOtp(resendOtp).ConfigureAwait(false);
            return StatusCode(
                (!resendOtpResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK),
                new
                {
                    resendOtpResponse.Status,
                    resendOtpResponse.Message
                });
        }

        [AllowAnonymous, HttpPost, Route("otp/verify")]
        public async Task<IActionResult> VerifyOtp([FromBody] DtoVerifyOtp verifyOtp)
        {
            var verifyOtpResponse = await _accountService.VerifyOtp(verifyOtp).ConfigureAwait(false);
            if (verifyOtpResponse.Status && !string.IsNullOrWhiteSpace(verifyOtpResponse.Token))
            {
                SetTokenInCookie(verifyOtpResponse.Token);
            }

            return StatusCode(
                (!verifyOtpResponse.Status ? StatusCodes.Status500InternalServerError : StatusCodes.Status200OK),
                new
                {
                    verifyOtpResponse.Status,
                    verifyOtpResponse.Message
                });
        }


        [HttpPost, Route("password/change")]
        public async Task<IActionResult> ChangePasswordAsync([FromBody] ChangePasswordRequest changePassword)
        {
            var changePasswordResponse = await _accountService.ChangePassword(changePassword).ConfigureAwait(false);

            return StatusCode(
                (!changePasswordResponse.Status ? StatusCodes.Status409Conflict : StatusCodes.Status200OK),
                new
                {
                    changePasswordResponse.Status,
                    changePasswordResponse.Message,
                });
        }


        [HttpGet, Route("logout"), Authorize]
        public async Task<IActionResult> LogoutAsync()
        {
            DeleteTokenFromCookie();

            var response = await _authService.Logout("[Account].[uspUserLogout]", new
            {
                UserId = GetUserId(),

            }).ConfigureAwait(false);

            if (!response.Status)
            {
                return BadRequest(response);
            }

            return Ok(response);
        }




    }
}
