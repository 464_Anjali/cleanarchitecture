﻿using BoilerplateCleanArchitectureV3.Application.Interfaces.ManageUsers;
using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using BoilerplateCleanArchitectureV3.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BoilerplateCleanArchitectureV3.Controllers
{
    [Route("api/manageusers")]
    //[ApiController]
    public class ManageUsersController : BaseController
    {
        private readonly IUserService _userService;
        public ManageUsersController(IUserService userService) 
        { 
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> UserAdd([FromBody] UsersAddRequest usersAdd)
        {
            var response = await _userService.UserAdd(usersAdd).ConfigureAwait(false);
            return response.Status ? StatusCode(StatusCodes.Status201Created, response) : StatusCode(StatusCodes.Status409Conflict, response);
        }

        [HttpPut]
        public async Task<IActionResult> UserUpdate([FromBody] UserUpdateRequest userUpdate)
        {
            var response = await _userService.UserUpdate(userUpdate).ConfigureAwait(false);
            return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status409Conflict, response);
        }

        [HttpDelete]
        public async Task<IActionResult> UserDelete([FromQuery] UserDeleteRequest userDelete)
        {
            var response = await _userService.UserDelete(userDelete).ConfigureAwait(false);
            return response.Status ? StatusCode(StatusCodes.Status204NoContent, response) : StatusCode(StatusCodes.Status404NotFound, response);
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> UserGet(string userId)
        {
            var response = await _userService.UserDetailsGet(userId).ConfigureAwait(false);
            return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status404NotFound, response);
        }

        [HttpGet]
        public async Task<IActionResult> UserList([FromQuery] UserListRequest userList)
        {
            var response = await _userService.UserListGet(userList).ConfigureAwait(false);
            return response.Status ? StatusCode(StatusCodes.Status200OK, response) : StatusCode(StatusCodes.Status404NotFound, response);
        }
    }
}
