﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace BoilerplateCleanArchitectureV3.APIServices.Filter
{
    public class ValidationFilter : ActionFilterAttribute
    {
        public override Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.ModelState.IsValid)
            {
                var message = string.Join(Environment.NewLine, context.ModelState.Values
                                            .SelectMany(x => x.Errors)
                                            .Select(x => x.ErrorMessage));
                context.Result = new BadRequestObjectResult(new { status = false, message });
            }

            return base.OnActionExecutionAsync(context, next);
        }
    }
}
