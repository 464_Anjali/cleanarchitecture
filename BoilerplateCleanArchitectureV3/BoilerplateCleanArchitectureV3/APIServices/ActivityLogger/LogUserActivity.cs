using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace BoilerplateCleanArchitectureV3.APIServices.ActivityLogger
{

    public class LogUserActivity
    {

      
        public string? UserId { get; set; }
        public string? IpAddress { get; set; }
        public string? AreaAccessed { get; set; }
        public string? TimeStamp { get; set; }
        public string? Body { get; set; }
        public string? StatusCode { get; set; }
        public string? Method { get; set; }
        public string? Discription { get; set; }
    }

    public class UserActivity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string?  UserId { get; set; }
        public string? RequestedFor { get; set; }
        public string? Message { get; set; }
    }
}
