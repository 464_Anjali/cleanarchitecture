
namespace BoilerplateCleanArchitectureV3.APIServices.ActivityLogger
{
    public interface ILogUserActivityRepository
    {
        void SaveLog(LogUserActivity logUserActivity);
    }
}