
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using MongoDB.Driver;
using BoilerplateCleanArchitectureV3.Domain;
//using MyWebAPIBoilerPlateV3.Core.Common;

namespace BoilerplateCleanArchitectureV3.APIServices.ActivityLogger
{
    public class ActivityLogFilterAttribute : ActionFilterAttribute
    {
        private readonly IConfiguration _configuration;
        private readonly MongoClient _mongoClient;
        private readonly IMongoCollection<LogUserActivity> _mongoCollection;
        private static string? Jsonrequest;

        public ActivityLogFilterAttribute(IConfiguration configuration)
        {
            _configuration = configuration;
            var _mongoClient = new MongoClient(_configuration["MongoDbSettings:ConnectionString"]);
            var databaseName = (_configuration["MongoDbSettings:DatabaseName"]);
            if (string.IsNullOrEmpty(databaseName))
            {
                throw new ArgumentNullException("DatabaseName is missing or null in configuration.");
            }
            var mongoDB = _mongoClient.GetDatabase(databaseName);
            _mongoCollection = mongoDB.GetCollection<LogUserActivity>(_configuration["MongoDbSettings:CollectionName"]);
        }


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext?.ActionArguments;
            Jsonrequest = JsonConvert.SerializeObject(request);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {

            // Capture response details
            ResponseObj responseObj = new();
            try
            {
                var request = filterContext?.HttpContext.Request;
            
                if (filterContext?.Result is ObjectResult objResponse)
                {

                    var jsonResponse = JsonConvert.SerializeObject(objResponse.Value);

                    responseObj = JsonConvert.DeserializeObject<ResponseObj>(jsonResponse)!;
                }
                else
                {
                    responseObj = new ResponseObj() { Message = "File downloaded." }; // Adjust this as needed
                }

                // Generate the log of user activity
                var log = new LogUserActivity()
                {
                    UserId = Guid.Parse(filterContext?.HttpContext?.User?.Identity?.Name ?? "F1673AE9-D195-4DAC-A516-46F56DE6791E").ToString(),
                    IpAddress = request?.HttpContext?.Connection?.RemoteIpAddress?.ToString(),
                    AreaAccessed = request != null ? Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(request).ToString() : "",
                    TimeStamp = DateTime.UtcNow.ToString(),
                    Body = Jsonrequest,                 
                    StatusCode = filterContext?.HttpContext.Response.StatusCode.ToString()!,
                    Method = request?.Method.ToString(),
                    Discription = responseObj?.Message!.ToString()
                };

                // Save log data to MongoDB
                _mongoCollection.InsertOne(log);

                base.OnResultExecuted(filterContext!);
            }
            catch (Exception ex)
            {
                responseObj.Status = false;
               responseObj.Message = "Error Occured : " + ex.Message;
            }
        }


    }
}

//        private readonly ICrudOperationService _crudOperationService;
//        private static string? Jsonrequest;

//        public ActivityLogFilterAttribute(ICrudOperationService crudOperationService)
//        {
//            _crudOperationService = crudOperationService;
//        }

//        public override void OnActionExecuting(ActionExecutingContext filterContext)
//        {
//            var request = filterContext?.ActionArguments;
//            Jsonrequest = JsonConvert.SerializeObject(request);
//        }

//        public override void OnResultExecuted(ResultExecutedContext filterContext)
//        {
//            try
//            {
//                ResponseObj responseObj = new();
//                var response = filterContext?.HttpContext.Response;
//                var request = filterContext?.HttpContext.Request;
//                if (filterContext?.Result is not FileContentResult)
//                {
//                    if (filterContext?.Result is ObjectResult Objresponse)
//                    {
//                        var Jsonresponse = JsonConvert.SerializeObject(Objresponse.Value);
//                        responseObj = JsonConvert.DeserializeObject<ResponseObj>(Jsonresponse)!;
//                    }
//                }
//                else
//                {
//                    var Jsonresponse = JsonConvert.SerializeObject(filterContext.Result);
//                    responseObj = JsonConvert.DeserializeObject<ResponseObj>(Jsonresponse)!;
//                    responseObj = new Core.Common.ResponseObj() { Message = responseObj?.FileDownloadName ?? "" + " file " };
//                }  
//                // Generate the log of user activity
//                LogUserActivity log = new LogUserActivity()
//                {
//                    UserId = Guid.Parse(filterContext?.HttpContext?.User?.Identity?.Name ?? "F1673AE9-D195-4DAC-A516-46F56DE6791E"),
//                    IpAddress = request?.HttpContext?.Connection?.RemoteIpAddress?.ToString(),
//                    AreaAccessed = request != null ? Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(request) : "",
//                    TimeStamp = DateTime.UtcNow,
//                    Body = Jsonrequest,
//                    StatusCode = filterContext?.HttpContext.Response.StatusCode,
//                    Method = request?.Method,
//                    Discription = responseObj?.Message != null ? Convert.ToString(responseObj?.Message, CultureInfo.CurrentCulture) : ""//responseObj.message.ToString()
//                };
//                //saves the log to database
//                 _crudOperationService.InsertUpdateDelete<CrudOperations.Response>("[Log].[UspUserAuditloggerSave]", log).ConfigureAwait(false);

//                if (filterContext != null)
//                    base.OnResultExecuted(filterContext);
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//        }
//    }
//}
