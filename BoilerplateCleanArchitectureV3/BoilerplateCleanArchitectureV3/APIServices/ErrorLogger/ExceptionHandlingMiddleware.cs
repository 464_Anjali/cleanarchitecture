﻿using BoilerplateCleanArchitectureV3.Application;
using CrudOperations;
using log4net;
using log4net.Config;
using System.Globalization;
using System.Net;
using System.Text.Json;

namespace BoilerplateCleanArchitectureV3.APIServices.ErrorLogger
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(ExceptionHandlingMiddleware));

        public ExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
            XmlConfigurator.Configure();
        }

        public async Task Invoke(HttpContext context)
        {
            var originalRequestBody = context.Request.Body;
            try
            {
                using var requestBodyStream = new MemoryStream();

                await context.Request.Body.CopyToAsync(requestBodyStream);
                requestBodyStream.Seek(0, SeekOrigin.Begin);

                var requestBody = await new StreamReader(requestBodyStream).ReadToEndAsync();

                _logger.InfoFormat("Request Body: {0}", requestBody);
                requestBodyStream.Seek(0, SeekOrigin.Begin);
                context.Request.Body = requestBodyStream;

                await _next(context);
            }
            catch (Exception error)
            {
                var response = context?.Response;

                if (response != null)
                {
                    response.ContentType = "application/json";
                    response.StatusCode = error switch
                    {
                        NullReferenceException _ => (int)HttpStatusCode.InternalServerError,
                        KeyNotFoundException _ => (int)HttpStatusCode.NotFound,
                        UnauthorizedAccessException _ => (int)HttpStatusCode.Unauthorized,
                        InvalidOperationException _ => (int)HttpStatusCode.BadRequest,
                        // Add More exception types here
                        _ => (int)HttpStatusCode.InternalServerError,
                    };

                    string resultMessage = Utilty.GetDataFromXML("ErrorMessage.xml", "error", "internal-server-error-message") ?? "";

                    Response responseModel = new() { Status = false, Message = error.Message };

                    if (response.StatusCode == (int)HttpStatusCode.InternalServerError)
                        responseModel.Message = $"{responseModel.Message} {resultMessage}";

                    var result = JsonSerializer.Serialize(responseModel);
                    string? controller = Convert.ToString(context?.Request.RouteValues["controller"], CultureInfo.CurrentCulture);
                    string? action = Convert.ToString(context?.Request?.RouteValues["action"], CultureInfo.CurrentCulture);

                    _logger.ErrorFormat("Error in {0} controller, {1} action. User: {2}. Error Stack Trace: {3}, Error Message {4}",
                    controller ?? "", action ?? "", context?.User?.Identity?.Name ?? "75AA3AAC-19B4-43D4-B4E2-F612E4228A6B", error.StackTrace, error.Message);

                    if (result != null)
                        await response.WriteAsync(result).ConfigureAwait(false);
                }
            }

            finally
            {
                context.Request.Body = originalRequestBody;
            }
        }
    }
}
