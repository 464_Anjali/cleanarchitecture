﻿using Authentication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Text.Encodings.Web;

namespace BoilerplateCleanArchitectureV3.APIServices.CustomAuthenticationHandler
{
    public class CustomAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IAuthService _authService;

        public CustomAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options,
                                           ILoggerFactory logger,
                                           UrlEncoder encoder,
                                           ISystemClock clock,
                                           IAuthService authenticationService)
                                           : base(options, logger, encoder, clock)
        {
            _authService = authenticationService;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var endpoint = Context.GetEndpoint();
            AuthenticateResult result;
            if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null)
            {
                return AuthenticateResult.NoResult();
            }

            if (!Request.Cookies.TryGetValue("AuthToken", out var encryptedToken) || string.IsNullOrWhiteSpace(encryptedToken))
            {
                return AuthenticateResult.Fail("Missing or empty AuthToken cookie.");
            }

            var token = _authService.Decrypt(encryptedToken);

            if (string.IsNullOrWhiteSpace(token))
            {
                return AuthenticateResult.Fail("Invalid Token: Unable to decrypt.");
            }
            //var endpoint = Context.GetEndpoint();
            //AuthenticateResult result;
            //if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() != null)
            //{
            //    return AuthenticateResult.NoResult();
            //}

            //string authorizationHeader = Request.Headers["Authorization"]!;

            //if (string.IsNullOrEmpty(authorizationHeader))
            //{
            //    return AuthenticateResult.Fail("Missing Authorization Header");
            //}

            //if (!authorizationHeader.StartsWith("Bearer ", StringComparison.OrdinalIgnoreCase))
            //{
            //    return AuthenticateResult.Fail("Invalid Authorization Header");
            //}

            //string token = authorizationHeader["Bearer ".Length..].Trim();

            //if (string.IsNullOrEmpty(token))
            //{
            //    return AuthenticateResult.Fail("Invalid Bearer Token");
            //}

            try
            {
                ClaimsPrincipal? principal = await _authService.GetPrincipal(token);
                if (principal is not null)
                {
                    var ticket = new AuthenticationTicket(principal!, Scheme.Name);
                    result = AuthenticateResult.Success(ticket);
                }
                else
                {
                    result = AuthenticateResult.Fail("Invalid Token");
                }
            }
            catch (Exception ex)
            {
                result = AuthenticateResult.Fail(ex.Message);
            }

            return await Task.FromResult(result);
        }
    }
}
