﻿using Microsoft.Extensions.Configuration;
using System.Security.Claims;
namespace Authentication
{
    public interface IAuthService
    {
        string AppSettingsKeys(string nodeName, string key);
        IConfigurationSection GetConfigurationSection(string key);
        // Sign-up user
        Task<T?> SignUp<T>(string storeProcedureName, object spParametersClassObject);
        // Login
        Task<AuthResponse<T>> AuthenticateUser<T>(string storedProcedureName, object spParametersClassObject, string password , string invalidLoginAttemptCheckerSpName , bool isFromChangePassword = false);

        // check invalid attempts
        Task<InvalidAttemptResponse<T>> InvalidLoginAttemptChecker<T>(string storedProcedureName, object spParametersClassObject);

       // Get user details
       Task<AuthResponse<T>> UserBasicDetailsGetAsync<T>(string storedProcedureName, object spParametersClassObject);
        // Save Token
        Task<AuthResponse> SaveTokenAsync(string storeProcedureName, object spParametersClassObject);
        Task<AuthResponse<T>?> CheckValidEmailAsync<T>(string storedProcedureName, object spParametersClassObject);
        Task<ForgotPasswordResponse> ForgotPasswordAsync<T>(string checkValidEmailSpName, string saveEmailTokenSpName, object spParametersClassObject);
        Task<ForgotPasswordResponse> VerifyPasswordTokenAsync(string verifyEmailTokenSpName, object spParametersClassObject);
        Task<AuthResponse> ChangePasswordAsync<T>(string storeProcedureName, object spParametersClassObject, string logInSpName);
        Task<AuthResponse> ResetPasswordAsync(string storeProcedureName, object spParametersClassObject);

        // Validate one time password(OTP)
        Task<AuthResponse<T>> VerifyOneTimePassword<T>(string storeProcedureName, object spParametersClassObject, string userBasicDetailsGetSpName);

        //Logout 
        //Task<AuthResponse> Logout(string token , string storeProcedureName, object spParametersClassObject);
        Task<AuthResponse> Logout(string storeProcedureName, object spParametersClassObject);
        Task<string?> GenerateToken(string userId, string email, string role, int expireMinutes = 30); 
        Task<ClaimsPrincipal?> GetPrincipal(string token);
        Task<string?> GenerateSecureOTP(int length);

        string Encrypt(string input);
        string Decrypt(string cipherText);



    }
}
