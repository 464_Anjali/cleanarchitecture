﻿
namespace Authentication
{
    public class AuthResponse
    {
        public bool Status { get; set; }
        public string? Message { get; set; }
    }

    public class AuthResponse<T> : AuthResponse 
    {
        public string? Otp { get; set; }
        public string? Token { get; set; }
        public int InvalidAttemptAllowed { get; set; }
        public T? Data { get; set; }

    }

    public class AuthReponseList<T> : AuthResponse
    {
        public string? Link { get; set; }
        public string? Email { get; set; }
        public string? EmailToken { get; set; }
    }

    public class ForgotPasswordResponse : AuthResponse
    {
        public string? Link { get; set; }
        public string? Email { get; set; }
        public string? EmailToken { get; set; }
    }

    public class InvalidAttemptResponse<T> : AuthResponse
    {
        public T? Data { get; set; }
    }

}
