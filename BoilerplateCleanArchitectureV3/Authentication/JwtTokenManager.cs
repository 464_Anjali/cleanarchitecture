﻿using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.DataProtection;

namespace Authentication
{
    public class JwtTokenManager : TokenManager
    {
        private readonly string _jwtSecrectKey;
        public JwtTokenManager(IConfiguration configuration, IDataProtectionProvider dataProtectionProvider, string connectionString) : base(configuration, dataProtectionProvider, connectionString)
        {
            _jwtSecrectKey = _configuration["SecretKey"]!;
        }

        public override async Task<string?> GenerateToken(string userId, string email, string role, int expireMinutes = 30)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_jwtSecrectKey);

                var claims = new List<Claim>()
                {
                    new(ClaimTypes.Name, userId),
                    new(ClaimTypes.Role, role),
                    new(ClaimTypes.Email, email)
                };

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.UtcNow.AddMinutes(expireMinutes),
                    Audience = _audience ?? "",
                    Issuer = _issuer ?? "",
                    IssuedAt = DateTime.UtcNow,
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);
                return await Task.FromResult(tokenString);
            }
            catch
            {
                return null;
            }
        }

        public override async Task<ClaimsPrincipal?> GetPrincipal(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                var principal = tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = _issuer ?? "",
                    ValidateAudience = true,
                    ValidAudience = _audience ?? "",
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(_jwtSecrectKey))
                }, out SecurityToken validatedToken);

                return await Task.FromResult(principal);
            }
            catch
            {
                return null;
            }
        }

        public override async Task<AuthResponse> Logout(string storeProcedureName, object spParametersClassObject)
        {
            AuthResponse response = new();
            using var connections = Connection;
            try
            {
                //if (string.IsNullOrWhiteSpace(token))
                //{
                //    response.Message = "Empty token";
                //    return response;
                //}

                //var handler = new JwtSecurityTokenHandler();
                //JwtSecurityToken? jwtToken;
                //try
                //{
                //    jwtToken = handler.ReadToken(token.Split(" ")[1]) as JwtSecurityToken;
                //}
                //catch (Exception)
                //{
                //    response.Message = "Invalid token";
                //    return response;
                //}

                //if (jwtToken?.Claims?.FirstOrDefault() == null || string.IsNullOrWhiteSpace(jwtToken.Claims.FirstOrDefault()!.Value))
                //{
                //    response.Message = "Invalid token";
                //    return response;
                //}

                response = await connections.QueryFirstAsync<AuthResponse>(
                     sql: storeProcedureName,
                     param: spParametersClassObject,
                     commandTimeout: null,
                     commandType: CommandType.StoredProcedure);

                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State != ConnectionState.Closed)
                    connections.Close();
            }
        }
    }
}
