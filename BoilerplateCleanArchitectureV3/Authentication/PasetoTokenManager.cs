﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Security.Claims;
using Dapper;
using Paseto.Builder;
using Paseto;
using System.Text.Json;
using Microsoft.AspNetCore.DataProtection;

namespace Authentication
{
    public class PasetoTokenManager : TokenManager
    {
        private readonly byte[] _pasetoSecrectKey;
        public PasetoTokenManager(IConfiguration configuration, IDataProtectionProvider dataProtectionProvider, string connectionString) : base(configuration, dataProtectionProvider, connectionString)
        {
            _pasetoSecrectKey = Convert.FromBase64String(_configuration["SecretKey"]!);
        }

        public override async Task<string?> GenerateToken(string userId, string email, string role, int expireMinutes = 30)
        {
            try
            {
                var token = new PasetoBuilder().UseV4(Purpose.Local)
                                               .WithKey(_pasetoSecrectKey, Encryption.SymmetricKey)
                                               .AddClaim(ClaimTypes.Name, userId)
                                               .AddClaim(ClaimTypes.Email, email)
                                               .AddClaim(ClaimTypes.Role, role)
                                               .Issuer(_issuer ?? "")
                                               //.Subject(Guid.NewGuid().ToString())
                                               .Audience(_audience ?? "")
                                               .IssuedAt(DateTime.UtcNow)
                                               .Expiration(DateTime.UtcNow.AddMinutes(expireMinutes))
                                               .TokenIdentifier("");

                return await Task.FromResult(token.Encode());
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override async Task<ClaimsPrincipal?> GetPrincipal(string token)
        {
            try
            {
                PasetoTokenValidationResult result = new PasetoBuilder().UseV4(Purpose.Local)
                                                                        .WithKey(_pasetoSecrectKey, Encryption.SymmetricKey)
                                                                        .Decode(token);
                if (!result.IsValid)
                {
                    return null;
                }

                var payload = JsonSerializer.Deserialize<Dictionary<string, object>>(result.Paseto.Payload.ToJson());
                var claims = payload?.Select(kv => new Claim(kv.Key, kv.Value.ToString()!));
                ClaimsPrincipal principal = new(new ClaimsIdentity(claims, "paseto"));

                return await Task.FromResult(principal);
            }
            catch 
            { 
                return null; 
            }
        }

        public override async Task<AuthResponse> Logout(string storeProcedureName, object spParametersClassObject)
        {
            //token = Decrypt(token);
            AuthResponse response = new();
            using var connections = Connection;
            //try
            //{
            //    if (string.IsNullOrWhiteSpace(token))
            //    {
            //        response.Message = "Empty token";
            //        return response;
            //    }

            //    token = token["Bearer ".Length..].Trim();

                //try
                //{
                //    var result = new PasetoBuilder().UseV4(Purpose.Local)
                //                                          .WithKey(_pasetoSecrectKey, Encryption.SymmetricKey)
                //                                          .Decode(token);
                //    if (!result.IsValid)
                //    {
                //        response.Message = "Invalid Token";
                //        return response;
                //    }
                //}
                //catch
                //{
                //    response.Message = "Invalid Token";
                //    return response;
                //}

                response = await connections.QueryFirstAsync<AuthResponse>(
                     sql: storeProcedureName,
                     param: spParametersClassObject,
                     commandTimeout: null,
                     commandType: CommandType.StoredProcedure);

                return response;
            }
            //catch(Exception)
            //{
            //    throw;
            //}
       // }
    }
}
