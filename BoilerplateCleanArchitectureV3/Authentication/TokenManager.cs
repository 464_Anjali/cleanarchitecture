﻿using Dapper;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using BCryptNet = BCrypt.Net.BCrypt;

namespace Authentication
{
    public abstract class TokenManager : IAuthService
    {
        protected readonly IConfiguration _configuration; // Configuration for accessing application settings
        protected readonly IDataProtectionProvider _dataProtectionProvider;
        protected readonly string _connectionString; // Database connection string
        protected readonly string? _issuer;
        protected readonly string? _audience;
        protected const string Key = "bxl7NMlvyp7xORE45DqqvHXaB2sgI5lH";

        public TokenManager(IConfiguration configuration, IDataProtectionProvider dataProtectionProvider, string connectionString)
        {
            _configuration = configuration;
            _dataProtectionProvider = dataProtectionProvider;
            _connectionString = connectionString;
            _issuer = _configuration["Issuer"];
            _audience = _configuration["Audience"];
        }

        // Property to get a new instance of IDbConnection based on the connection string
        public IDbConnection Connection => new SqlConnection(_connectionString);

        // Method to retrieve a connection string by name from configuration
        public string GetConnectionString(string connectionName) => _configuration.GetConnectionString(connectionName)!;

        // Method to retrieve a configuration section by key
        public IConfigurationSection GetConfigurationSection(string key) => _configuration.GetSection(key);

        // Method to retrieve a configuration value based on a specific node and key using string interpolation
        public string AppSettingsKeys(string nodeName, string key) => _configuration[$"{nodeName}:{key}"]!;

        #region Sign Up
        /// <summary>
        /// Sign-up -> User can create their account by adding details 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storeProcedureName"></param>
        /// <param name="spParametersClassObject"></param>
        /// <returns></returns>
        public virtual async Task<T?> SignUp<T>(string storedProcedureName, object spParamsPocoMapper)
        {
            using var connections = Connection;
            try
            {
                return await connections.QueryFirstOrDefaultAsync<T>(
                    sql: storedProcedureName,
                    param: spParamsPocoMapper,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                    );
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }
        #endregion

        #region Login
        /// <summary>
        /// AuthenticateUser ->Asynchronously authenticates a user by executing a stored procedure.
        /// The method checks the user's password and optionally handles two-factor authentication.
        /// If authentication is successful, it generates a token or OTP as required.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storedProcedureName"></param>
        /// <param name="spParametersClassObject"></param>
        /// <param name="password"></param>
        /// <returns name="AuthResponse<T>"></returns>
        public virtual async Task<AuthResponse<T>> AuthenticateUser<T>(string storedProcedureName, object spParametersClassObject, string password , string invalidLoginAttemptCheckerSpName , bool isFromChangePassword = false)
        {
            using var connections = Connection;
            try
            {
                AuthResponse<T> response;
                var result = await connections.QueryMultipleAsync(
                    sql: storedProcedureName,
                    param: spParametersClassObject,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure);

                response = result.Read<AuthResponse<T>>().FirstOrDefault()!;

                if (!response.Status)
                {
                    response.Otp = default;
                    response.Token = default;
                    response.Data = default;
                    return response;
                }
                response.Data = result.Read<T>().FirstOrDefault();

                var email = (string)spParametersClassObject.GetType().GetProperty("Email")!.GetValue(spParametersClassObject)!;

                if (!VerifyPasswordHash(password, (byte[])typeof(T).GetProperty("PasswordHash")!.GetValue(response.Data, null)!, (byte[])typeof(T).GetProperty("PasswordSalt")!.GetValue(response.Data, null)!))
                {   
                    if(isFromChangePassword)
                    {
                        response.Status = false;
                        response.Message = "password is not valid!";
                        return response;

                    }
                    var invalidAttemptResponse = await InvalidLoginAttemptChecker<int>(invalidLoginAttemptCheckerSpName , new {Email = email}).ConfigureAwait(false);

                    response.Status = false;
                    response.Message = invalidAttemptResponse.Message;
                    response.Otp = default;
                    response.Token = default;
                    response.InvalidAttemptAllowed = invalidAttemptResponse.Data;
                    response.Data = default;
                    return response;
                }
                else
                {
                    if (isFromChangePassword)
                    {
                        return response;
                    }
                    var invalidAttemptResponse = await InvalidLoginAttemptChecker<int>(invalidLoginAttemptCheckerSpName, new { Email = email, ClearInvalidAttemptCount = true }).ConfigureAwait(false);
                }


                if ((bool)typeof(T).GetProperty("IsTwoFactorEnable")!.GetValue(response.Data, null)!)
                {
                    string? otp = await GenerateSecureOTP(6);
                    if (otp is null)
                    {
                        response.Status = false;
                        response.Message = "Failed to generate otp";
                        response.Otp = default;
                        response.Token = default;
                        response.InvalidAttemptAllowed = default;
                        response.Data = default;
                        return response;
                    }
                    response.Otp = otp;
                    response.Token = default;
                    response.InvalidAttemptAllowed = default;
                }
                else
                {
                    string? token = await GenerateToken((string)typeof(T).GetProperty("UserId")!.GetValue(response.Data, null)!,
                                                        (string)typeof(T).GetProperty("Email")!.GetValue(response.Data, null)!,
                                                        (string)typeof(T).GetProperty("Role")!.GetValue(response.Data, null)!);
                    if (token is null)
                    {
                        response.Status = false;
                        response.Message = "Failed to generate token";
                        response.Otp = default;
                        response.Token = default;
                        response.InvalidAttemptAllowed = default;
                        response.Data = default;
                        return response;
                    }
                    response.Token = Encrypt(token);
                    response.Otp = default;
                }

                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }
        #endregion

        #region Check Invalid Attempt
        public virtual async Task<InvalidAttemptResponse<T>> InvalidLoginAttemptChecker<T>(string storedProcedureName, object spParametersClassObject)
        {
            using var connections = Connection;
            try
            {
                var result = await connections.QueryMultipleAsync(
                    sql: storedProcedureName,
                    param: spParametersClassObject,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                    );

                var response = result.Read<InvalidAttemptResponse<T>>().FirstOrDefault()!;
                response.Data = response.Status ? result.Read<T>().FirstOrDefault() : default;

                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }

        }
        #endregion

        #region Get User Details
        public virtual async Task<AuthResponse<T>> UserBasicDetailsGetAsync<T>(string storedProcedureName, object spParametersClassObject)
        {
            using var connections = Connection;
            try
            {
                var result = await connections.QueryMultipleAsync(
                    sql: storedProcedureName,
                    param: spParametersClassObject,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                    );

                var response = result.Read<AuthResponse<T>>().FirstOrDefault()!;
                response.Data = response.Status ? result.Read<T>().FirstOrDefault() : default;
                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }

        }

        #endregion

        #region Save Token
        /// <summary>
        /// SaveTokenAsync -> It will specifically save the token of any user based on their email or userId.
        /// </summary>
        /// <param name="storeProcedureName"></param>
        /// <param name="spParametersClassObject"></param>
        /// <returns></returns>
        public virtual async Task<AuthResponse> SaveTokenAsync(string storedProcedureName, object spParametersObject)
        {
            using var connections = Connection;
            try
            {
                return await connections.QuerySingleAsync<AuthResponse>(
                        sql: storedProcedureName,
                        param: spParametersObject,
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }
        #endregion

        #region Check Valid Email

        /// <summary>
        /// CheckValidEmailAsync  -> It will check whether the email exists in the DB or not and if yes then what is the status.
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <param name="spParametersClassObject"></param>
        /// <returns></returns>
        public virtual async Task<AuthResponse<T>?> CheckValidEmailAsync<T>(string storedProcedureName, object spParametersClassObject)
        {
            using var connections = Connection;
            try
            {
                return await connections.QueryFirstOrDefaultAsync<AuthResponse<T>>(
                        sql: storedProcedureName,
                        param: spParametersClassObject,
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }
        #endregion

        #region ForgotPassword
        /// <summary>
        /// ForgotPasswordAsync -> If in any case the user forgets his password, he can reset his password based on their email.
        /// </summary>
        /// <param name="storeProcedureName"></param>
        /// <param name="spParametersClassObject"></param>
        /// <param name="checkValidEmailSpName"></param>
        /// <param name="forgotPswValidatorSpName"></param>
        /// <returns></returns>

        public virtual async Task<ForgotPasswordResponse> ForgotPasswordAsync<T>(string checkValidEmailSpName, string saveEmailTokenSpName, object spParametersClassObject)
        {
            ForgotPasswordResponse response = new();
            var validEmailResponse = await CheckValidEmailAsync<string>(checkValidEmailSpName, spParametersClassObject).ConfigureAwait(false);

            if (!validEmailResponse!.Status)
            {
                response.Status = false;
                response.Message = validEmailResponse.Message;
                response.Link = default;
                response.Email = default;
                response.EmailToken = default;
                return response;
            }
            string email = validEmailResponse.Data!;
            string? emailToken = await GenerateSecureOTP(10).ConfigureAwait(false);

            var tokenResponse = await SaveTokenAsync(saveEmailTokenSpName, new { Email = email, Token = emailToken }).ConfigureAwait(false);
            if (!tokenResponse.Status)
            {
                response.Status = false;
                response.Message = tokenResponse.Message;
                response.Link = default;
                response.Email = default;
                response.EmailToken = default;
                return response;
            }

            var encryptedEmail = Encrypt(email!);
            var encryptedEmailToken = Encrypt(emailToken!);

            string? link = _configuration["AppSettings:VerifyPasswordUrl"]?.Replace("{{EMAIL}}", encryptedEmail, StringComparison.InvariantCulture)
                                                       .Replace("{{TOKEN}}", encryptedEmailToken, StringComparison.InvariantCulture);

            response.Status = true;
            response.Message = "Forgot password successful!";
            response.Link = link;
            response.Email = encryptedEmail ?? "";
            response.EmailToken = encryptedEmailToken ?? "";

            return response;

        }

        #endregion

        #region Verify PasswordToken

        /// <summary>
        /// VerifyPasswordTokenAsync -> It will check all the conditions of the password sent by the user exists or not etc. It will be 2 factor authentication or 1 factor depends).
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <param name="spParametersClassObject"></param>
        /// <param name="forgotPswValidatorSpName"></param>
        /// <param name="userBasicDetailSpName"></param>
        /// <param name="T to get the user details"></param>
        /// <returns></returns>
        public virtual async Task<ForgotPasswordResponse> VerifyPasswordTokenAsync(string verifyEmailTokenSpName, object spParametersClassObject)
        {
            ForgotPasswordResponse forgotPasswordResponse = new();

            var email = Decrypt((string)spParametersClassObject.GetType().GetProperty("Email")!.GetValue(spParametersClassObject)! ?? "");
            var emailToken = Decrypt((string)spParametersClassObject.GetType().GetProperty("EmailToken")!.GetValue(spParametersClassObject)! ?? "");

            if (string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(emailToken))
            {
                forgotPasswordResponse.Status = false;
                forgotPasswordResponse.Message = "Failed to get email or emailtoken";
                forgotPasswordResponse.Email = default;
                forgotPasswordResponse.EmailToken = default;
                return forgotPasswordResponse;
            }

            var verifyTokenResponse = await SaveTokenAsync(verifyEmailTokenSpName, new { Email = email, EmailToken = emailToken }).ConfigureAwait(false); ;

            if (!verifyTokenResponse.Status)
            {
                forgotPasswordResponse.Status = false;
                forgotPasswordResponse.Message = verifyTokenResponse.Message;
                forgotPasswordResponse.Email = default;
                forgotPasswordResponse.EmailToken = default;
                return forgotPasswordResponse;
            }

            forgotPasswordResponse.Status = true;
            forgotPasswordResponse.Message = "Forgot password verification successful";
            forgotPasswordResponse.Email = email;
            forgotPasswordResponse.EmailToken = emailToken;

            return forgotPasswordResponse;
        }
        #endregion

        #region Change Password
        /// <summary>
        ///  ChangePassword: Asynchronously changes a user's password. It first authenticates the user with the current password
        ///  and then ensures the new password is not the same as the current password before changing it.  
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="storeProcedureName"></param>
        /// <param name="spParametersClassObject"></param>
        /// <param name="logInSpName"></param>
        /// <param name="logInSpParametersClassObject"></param>
        /// <param name="currentPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public virtual async Task<AuthResponse> ChangePasswordAsync<T>(string storeProcedureName, object spParametersClassObject, string logInSpName)
        {
            AuthResponse response = new();
            //var userId = (string)spParametersClassObject.GetType().GetProperty("UserId")!.GetValue(spParametersClassObject)!;
            var email = (string)spParametersClassObject.GetType().GetProperty("Email")!.GetValue(spParametersClassObject)!;
            var updatedBy = (string)spParametersClassObject.GetType().GetProperty("UpdatedBy")!.GetValue(spParametersClassObject)!;
            var currentPassword = (string)spParametersClassObject.GetType().GetProperty("OldPassword")!.GetValue(spParametersClassObject)!;
            var newPassword = (string)spParametersClassObject.GetType().GetProperty("NewPassword")!.GetValue(spParametersClassObject)!;

            var loginResult = await AuthenticateUser<T>(logInSpName, new {Email = email}, currentPassword , "" , true).ConfigureAwait(false);
            if (!loginResult.Status)
            {
                response.Status = false;
                response.Message = "Current password does not match";
                return response;
            }
            var logInNewRes = await AuthenticateUser<T>(logInSpName, new { Email = email}, newPassword , "" , true);
            if (logInNewRes.Status)
            {
                response.Status = false;
                response.Message = "New password cannot be same as current password";
                return response;
            }


            CreatePasswordHash(newPassword, out byte[] passwordHash, out byte[] passwordSalt);

            var result = await SignUp<AuthResponse>(storeProcedureName, new
            {
                //UserId = userId,
                Email = email,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                UpdatedBy = updatedBy
            }).ConfigureAwait(false);

            return result!;
        }
        #endregion

        #region Reset Password
        public virtual async Task<AuthResponse> ResetPasswordAsync(string storeProcedureName, object spParametersClassObject)
        {
            //var userId = (string)spParametersClassObject.GetType().GetProperty("UserId")!.GetValue(spParametersClassObject)!;
            var email = (string)spParametersClassObject.GetType().GetProperty("Email")!.GetValue(spParametersClassObject)!;
            var password = (string)spParametersClassObject.GetType().GetProperty("Password")!.GetValue(spParametersClassObject)!;
            var updatedBy = (Guid)spParametersClassObject.GetType().GetProperty("UpdatedBy")!.GetValue(spParametersClassObject)!;

            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            var result = await SignUp<AuthResponse>(storeProcedureName, new
            {
                Email = email,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                UpdatedBy = updatedBy
            }).ConfigureAwait(false);

            return result!;

        }
        #endregion

        #region Verify OTP
        public virtual async Task<AuthResponse<T>> VerifyOneTimePassword<T>(string storeProcedureName, object spParametersClassObject, string userBasicDetailsGetSpName)
        {
            using var connections = Connection;
            try
            {
                var result = await connections.QueryMultipleAsync(
                    sql: storeProcedureName,
                    param: spParametersClassObject,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure);

                var response = result.Read<AuthResponse<T>>().FirstOrDefault()!;
                if (response.Status)
                {
                    var userId = (string)spParametersClassObject.GetType().GetProperty("UserId")!.GetValue(spParametersClassObject)!;
                    var userDetailsResponse = await UserBasicDetailsGetAsync<T>(userBasicDetailsGetSpName, new { UserId = userId }).ConfigureAwait(false);
                    if (userDetailsResponse.Status)
                    {
                        string? token = await GenerateToken(userId.ToString(),
                                                      (string)typeof(T).GetProperty("Email")!.GetValue(userDetailsResponse.Data, null)!,
                                                      (string)typeof(T).GetProperty("Role")!.GetValue(userDetailsResponse.Data, null)!);

                        if (token is null)
                        {
                            response.Status = false;
                            response.Message = "Failed to generate token";
                            response.Otp = default;
                            response.Token = default;
                            response.Data = default;
                            return response;
                        }
                        response.Token = Encrypt(token);
                        response.Otp = default;
                    }
                }
                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }
        #endregion

        #region Logout
        public abstract Task<AuthResponse> Logout(string storeProcedureName, object spParametersClassObject);
        #endregion

        // GenerateToken: An abstract method to be implemented in derived classes for token generation.
        public abstract Task<string?> GenerateToken(string userId, string email, string role, int expireMinutes = 30);

        // GetPrincipal: An abstract method to be implemented in derived classes to get the principal from a token.
        public abstract Task<ClaimsPrincipal?> GetPrincipal(string token);

        #region Generate Otp
        public async Task<string?> GenerateSecureOTP(int length)
        {
            try
            {
                await Task.Yield();
                const string chars = "0123456789";
                var otpChars = new char[length];
                var uintBuffer = new byte[4];

                for (int i = 0; i < length; i++)
                {
                    RandomNumberGenerator.Fill(uintBuffer);
                    uint num = BitConverter.ToUInt32(uintBuffer, 0);
                    otpChars[i] = chars[(int)(num % chars.Length)];
                }
                return new string(otpChars);
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Common Methods

        public delegate Task<T> QueryExecutionDelegate<T>(IDbConnection connection, string storedProcedureName, object parameters);
        public async Task<T> ExecuteQuery<T>(string storedProcedureName, object parameters, QueryExecutionDelegate<T> executeQuery)
        {
            using var connection = Connection;
            try
            {
                connection.Open();
                return await executeQuery(connection, storedProcedureName, parameters);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }

        public static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            var mySalt = BCryptNet.GenerateSalt();
            passwordSalt = Encoding.ASCII.GetBytes(mySalt);
            passwordHash = Encoding.ASCII.GetBytes(BCryptNet.HashPassword(password, mySalt));
        }
        public static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            var newPassword = BCryptNet.HashPassword(password, Encoding.ASCII.GetString(storedSalt));
            return (newPassword == Encoding.Default.GetString(storedHash));
        }
        
        public string Encrypt(string input)
        {
            var protector = _dataProtectionProvider.CreateProtector(Key);
            return protector.Protect(input);
        }

        public string Decrypt(string cipherText)
        {
            var protector = _dataProtectionProvider.CreateProtector(Key);
            return protector.Unprotect(cipherText);
        }

      
        #endregion

    }
}

