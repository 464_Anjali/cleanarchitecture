﻿using BoilerplateCleanArchitectureV3.Application.Interfaces.ManageUsers;
using BoilerplateCleanArchitectureV3.Domain.DtoModels;
using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using CrudOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Infrastructure
{
    public class UserRepository : IUserRepository
    {
        private readonly ICrudOperationService _crudOperationService;
        public UserRepository(ICrudOperationService crudOperationService) 
        {
            _crudOperationService = crudOperationService;
        }

        public async Task<Response> UserAddAsync(UsersAddRequest usersAdd)
        {
            return await _crudOperationService.InsertUpdateDelete<Response>("[ManageUser].[UspUserInsert]", usersAdd);
        }

        public async Task<Response> UserUpdateAsync(UserUpdateRequest userUpdate)
        {
            return await _crudOperationService.InsertUpdateDelete<Response>("[ManageUser].[UspUserUpdate]", userUpdate);
        }

        public async Task<Response> UserDeleteAsync(UserDeleteRequest userDelete)
        {
            return await _crudOperationService.InsertUpdateDelete<Response>("[ManageUser].[UspUserDelete]", userDelete);
        }

        public async Task<Response<UserDetailsResponse>> UserDetailsGetAsync(string userId)
        {
            DtoUserDetailsGet userDetailsGet = new DtoUserDetailsGet()
            {
                UserId = userId,
            };

            return await _crudOperationService.GetSingleRecord<UserDetailsResponse>("[ManageUser].[UspUserGet]", userDetailsGet);
        }

        public async Task<ResponseList<UserDetailsResponse>> UserListGetAsync(UserListRequest userList)
        {
            return await _crudOperationService.GetPaginatedList<UserDetailsResponse>("[ManageUser].[UspUsersGetList]", userList);
        }

        
    }
}
