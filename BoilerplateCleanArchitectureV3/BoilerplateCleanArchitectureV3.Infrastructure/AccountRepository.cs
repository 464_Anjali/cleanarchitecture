﻿using Authentication;
using BoilerplateCleanArchitectureV3.Application.Interfaces.Account;
using BoilerplateCleanArchitectureV3.Domain.DtoModels;
using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using CommonComponent.Email;
using CrudOperations;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Infrastructure
{
    public class AccountRepository : IAccountRepository
    {
        private readonly IAuthService _authService;
        private readonly ICrudOperationService _crudOperationService;


        public AccountRepository(   IAuthService authService,
                                    ICrudOperationService crudOperationService
                                )
        {
            _authService = authService;
            _crudOperationService = crudOperationService;
        }

        public async Task<Response> AccountSaveAsync(DtoAccountCreation accountCreationRequest)
        {
            return await _authService.SignUp<Response?>("[Account].[uspAccountCreation]", accountCreationRequest);
        }

        public async Task<AuthResponse<UserBasicDetailsResponse>> SignInAsync(DtoLogin dtoLogin)
        {
            return await _authService.AuthenticateUser<UserBasicDetailsResponse>("[Account].[uspUserLoginGet]", new { dtoLogin.Email }, dtoLogin.Password!, "[Account].[uspInvalidLoginAttemptChecker]").ConfigureAwait(false);
        }

        public async Task<AuthResponse> SaveOtpAsync(DtoSaveOtp dtoSaveOtp)
        {
           return await _authService.SaveTokenAsync("[Account].[uspSaveOtp]", dtoSaveOtp).ConfigureAwait(false);
        }

        public async Task<AuthResponse<UserBasicDetailsResponse>> UserBasicDetailsGetAsync(DtoUserBasicDetails dtoUserBasicDetails )
        {
            return await _authService.UserBasicDetailsGetAsync<UserBasicDetailsResponse>("[Account].[uspUserBasicDetailsGet]", dtoUserBasicDetails).ConfigureAwait(false);
        }

        public async Task<ForgotPasswordResponse> ForgotPasswordAsync(ForgotPasswordRequest forgotPassword)
        {
            return await _authService.ForgotPasswordAsync<string>("[Account].[uspCheckValidEmail]", "[Account].[uspSavePasswordVerifyToken]", forgotPassword).ConfigureAwait(false);
        }

        public async Task<ForgotPasswordResponse> VerifyPasswordTokenAsync(PasswordVerifyTokenRequest passwordVerifyTokenRequest)
        {
           return await _authService.VerifyPasswordTokenAsync("[Account].[uspVerifyPasswordToken]", passwordVerifyTokenRequest).ConfigureAwait(false);
        }

        public async Task<AuthResponse> ResetPasswordAsync(ResetPasswordRequest resetPassword)
        {
            return await _authService.ResetPasswordAsync("[Account].[uspUserResetPassword]", resetPassword).ConfigureAwait(false);
        }

        public async Task<AuthResponse<UserBasicDetailsResponse>> VerifyOtpAsync(DtoVerifyOtp dtoVerifyOtp)
        {
            return await _authService.VerifyOneTimePassword<UserBasicDetailsResponse>("[Account].[uspVerifyOTP]", dtoVerifyOtp, "[Account].[uspUserBasicDetailsGet]").ConfigureAwait(false);
        }

        public async Task<AuthResponse> ChangePasswordAsync(ChangePasswordRequest dtochangePassword)
        {
           return await _authService.ChangePasswordAsync<UserBasicDetailsResponse>("[Account].[uspUserResetPassword]", dtochangePassword, "[Account].[uspUserLoginGet]").ConfigureAwait(false);
        }
    }

}
       