﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Application
{
    public static class ListToTable
    {        public static System.Data.DataTable ConvertToDataTable<T>(List<T> items)
        {
            System.Data.DataTable dataTable = new System.Data.DataTable(typeof(T).Name);
            //Get all the properties by using reflection
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            if (items != null && items.Count > 0)
            {
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {
                        values[i] = Props[i].GetValue(item, null) ?? "";
                    }
                    dataTable.Rows.Add(values);
                }
            }
            return dataTable;
        }

        public static System.Data.DataTable AddClaimsToTable(List<string> Claims)
        {
            System.Data.DataTable dtClaims = new System.Data.DataTable();
            dtClaims.Columns.Add("ClaimId");
            foreach (var item in Claims)
            {
                dtClaims.Rows.Add(item);
            }
            return dtClaims;
        }

        public static System.Data.DataTable ConvertQuestionToDataTable<T>(List<T> items)
        {
            System.Data.DataTable dataTable = new System.Data.DataTable(typeof(T).Name);
            //Get all the properties by using reflection
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                if (prop.Name != "lstImportAnswers")
                {
                    //Setting column names as Property names
                    dataTable.Columns.Add(prop.Name);
                }
            }
            if (items != null && items.Count > 0)
            {
                foreach (T item in items)
                {
                    var values = new object[Props.Length - 1];
                    for (int i = 0; i < Props.Length - 1; i++)
                    {
                        values[i] = Props[i].GetValue(item, null) ?? "";
                    }
                    dataTable.Rows.Add(values);
                }
            }
            return dataTable;
        }
    }
}
