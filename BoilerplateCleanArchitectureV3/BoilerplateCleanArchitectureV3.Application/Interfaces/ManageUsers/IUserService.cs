﻿using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using CrudOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Application.Interfaces.ManageUsers
{
    public interface IUserService
    {
        Task<Response> UserAdd(UsersAddRequest usersAdd);
        Task<Response> UserUpdate(UserUpdateRequest userUpdate);
        Task<Response> UserDelete(UserDeleteRequest userDelete);
        Task<Response<UserDetailsResponse>> UserDetailsGet(string userId);
        Task<ResponseList<UserDetailsResponse>> UserListGet(UserListRequest userList);






    }
}
