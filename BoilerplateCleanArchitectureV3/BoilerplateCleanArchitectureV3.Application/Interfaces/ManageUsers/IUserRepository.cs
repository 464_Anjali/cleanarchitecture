﻿using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using CrudOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Application.Interfaces.ManageUsers
{
    public interface IUserRepository
    {
        Task<Response> UserAddAsync(UsersAddRequest usersAdd);
        Task<Response> UserUpdateAsync(UserUpdateRequest userUpdate);
        Task<Response> UserDeleteAsync(UserDeleteRequest userDelete);
        Task<Response<UserDetailsResponse>> UserDetailsGetAsync(string userId);
        Task<ResponseList<UserDetailsResponse>> UserListGetAsync(UserListRequest userList);

    }
}
