﻿using Authentication;
using BoilerplateCleanArchitectureV3.Domain.DtoModels;
using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using CrudOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Application.Interfaces.Account
{
    public interface IAccountService
    {
        Task<Response> AccountCreation(AccountCreationRequest accountCreationRequest);
        Task<AuthResponse<UserBasicDetailsResponse>> Login(DtoLogin login);
        Task<ForgotPasswordResponse> ForgotPassword(ForgotPasswordRequest forgotPasswordRequest);
        Task<ForgotPasswordResponse> VerifyPasswordToken(PasswordVerifyTokenRequest passwordVerifyTokenRequest);
        Task<AuthResponse> ResetPassword(ResetPasswordRequest  resetPassword);
        Task<AuthResponse<ResendOtpResponse>> ResendOtp(ResendOtpRequest  resendOtp);
        Task<AuthResponse<UserBasicDetailsResponse>> VerifyOtp(DtoVerifyOtp dtoVerifyOtp);
        Task<AuthResponse> ChangePassword(ChangePasswordRequest changePassword);

    }
}
