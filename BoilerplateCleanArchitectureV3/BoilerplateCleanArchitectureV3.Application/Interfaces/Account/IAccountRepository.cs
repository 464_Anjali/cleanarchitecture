﻿using Authentication;
using BoilerplateCleanArchitectureV3.Domain.DtoModels;
using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using CrudOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Application.Interfaces.Account
{
    public interface IAccountRepository
    {
        Task<Response> AccountSaveAsync(DtoAccountCreation accountCreationRequest);
        Task<AuthResponse<UserBasicDetailsResponse>> SignInAsync(DtoLogin dtoLogin);
        Task<AuthResponse> SaveOtpAsync(DtoSaveOtp dtoSaveOtp);
        Task<AuthResponse<UserBasicDetailsResponse>> UserBasicDetailsGetAsync(DtoUserBasicDetails dtoUserBasicDetails);
        Task<ForgotPasswordResponse> ForgotPasswordAsync(ForgotPasswordRequest forgotPassword);
        Task<ForgotPasswordResponse> VerifyPasswordTokenAsync(PasswordVerifyTokenRequest passwordVerifyTokenRequest);
        Task<AuthResponse> ResetPasswordAsync(ResetPasswordRequest resetPassword);
        Task<AuthResponse<UserBasicDetailsResponse>> VerifyOtpAsync(DtoVerifyOtp dtoVerifyOtp);
        Task<AuthResponse> ChangePasswordAsync(ChangePasswordRequest dtochangePassword);

    }
  
}
