﻿using BoilerplateCleanArchitectureV3.Application.Interfaces.ManageUsers;
using BoilerplateCleanArchitectureV3.Domain.DtoModels;
using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using CrudOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository) 
        {
            _userRepository = userRepository;
        }

        public async Task<Response> UserAdd(UsersAddRequest usersAdd)
        {
            return await _userRepository.UserAddAsync(usersAdd);
        }

        public async Task<Response> UserUpdate(UserUpdateRequest userUpdate)
        {
            return await _userRepository.UserUpdateAsync(userUpdate);
        }

        public async Task<Response> UserDelete(UserDeleteRequest userDelete)
        {
            return await _userRepository.UserDeleteAsync(userDelete);
        }

        public async Task<Response<UserDetailsResponse>> UserDetailsGet(string userId)
        {
           return await _userRepository.UserDetailsGetAsync(userId);
        }

        public async Task<ResponseList<UserDetailsResponse>> UserListGet(UserListRequest userList)
        {
           return await _userRepository.UserListGetAsync(userList);
        }
        
    }
}
