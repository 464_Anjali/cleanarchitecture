﻿using Authentication;
using BoilerplateCleanArchitectureV3.Application.Interfaces.Account;
using BoilerplateCleanArchitectureV3.Domain;
using BoilerplateCleanArchitectureV3.Domain.DtoModels;
using BoilerplateCleanArchitectureV3.Domain.ServiceModels;
using CommonComponent.Email;
using CrudOperations;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio.Http;

namespace BoilerplateCleanArchitectureV3.Application.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository  _accountRepository;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly ICrudOperationService _crudOperationService;
        private readonly IAuthService _authService;

        public AccountService(  IAccountRepository accountRepository,
                                IConfiguration configuration,
                                IEmailService emailService ,
                                ICrudOperationService crudOperationService,
                                IAuthService authService
                             )
                             
        {
            _accountRepository = accountRepository;        
            _configuration = configuration;
            _emailService = emailService;
            _crudOperationService = crudOperationService;
            _authService = authService;
        }

        public async Task<CrudOperations.Response> AccountCreation(AccountCreationRequest accountCreationRequest)
        {            
            PasetoTokenManager.CreatePasswordHash(accountCreationRequest.Password!, out byte[] passwordHash, out byte[] passwordSalt);
            DtoAccountCreation dtoCreateAccount = new ()
            {
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                FirstName = accountCreationRequest.FirstName,
                LastName = accountCreationRequest.LastName,    
                Email = accountCreationRequest.Email,
                RoleId = accountCreationRequest.RoleId,
                Mobile = accountCreationRequest.Mobile,
                CreatedBy = accountCreationRequest.CreatedBy,  
            };

            var response = await _accountRepository.AccountSaveAsync(dtoCreateAccount);
            return response;
        }

        public async Task<AuthResponse<UserBasicDetailsResponse>> Login(DtoLogin login)
        {
            var response =  await _accountRepository.SignInAsync(login);
            if (response.Status && response.Otp is not null)
            {

                DtoSaveOtp dtoSaveOtp = new DtoSaveOtp()
                {
                    Otp = response.Otp,
                    UserId = response.Data!.UserId,
                    IsTwoFactorEnable = response.Data.IsTwoFactorEnable,

                };

                var saveOtpResponse = await _accountRepository.SaveOtpAsync(dtoSaveOtp);
                if(!saveOtpResponse.Status)
                {
                    response.Status = false;
                    response.Message = saveOtpResponse.Message;
                    response.Data = default;
                }

                DtoUserBasicDetails userBasicDetails = new DtoUserBasicDetails()
                {
                    Email = login.Email,
                };
                var userDetailsResponse = await _accountRepository.UserBasicDetailsGetAsync(userBasicDetails);

                string body = Utilty.GetEmailTemplateBody("LoginOtpEmail.html");
                string? adminEmail = _configuration["AppSettings:AdminEmail"];
              
                body = body.Replace("{{otp}}", response.Otp)
               .Replace("{{username}}", userDetailsResponse.Data!.FirstName)
               .Replace("{{sendername}}", _configuration["AppSettings:CompanyNameMailSender"])!
               .Replace("{{adminemail}}", adminEmail)
               .Replace("{{company-name}}", _configuration["AppSettings:CompanyName"]);

                string mailSubject = EmailSubjectHelper.LoginOTPSubject.Replace("{{company-name}}", _configuration["AppSettings:CompanyName"]);
                _emailService?.SendEmail(login.Email ?? "", mailSubject, body, true);
            }

            else if (!response.Status && response.Message == "ACL")
            {
                DtoUserBasicDetails userBasicDetails = new DtoUserBasicDetails()
                {
                    Email = login.Email,
                };
                var userDetailsResponse = await _accountRepository.UserBasicDetailsGetAsync(userBasicDetails);


                string body = Utilty.GetEmailTemplateBody("AccountLockedEmail.html");
                string? userFirstName = userDetailsResponse.Data!.FirstName;
                string? invalidAttemptCount = Convert.ToString(response.InvalidAttemptAllowed, CultureInfo.CurrentCulture);


                body = body.Replace("{{FIRSTNAME}}", userFirstName)
                    .Replace("{{company-name}}", _configuration["AppSettings:CompanyName"])
                    .Replace("{{invalid_attempt}}", invalidAttemptCount);

                string mailSubject = EmailSubjectHelper.AccountLockSubject.Replace("{{company-name}}", _configuration["AppSettings:CompanyName"]);

                _emailService?.SendEmail(login.Email ?? "", mailSubject, body, true);

            }

            return response;

        }

        public  async Task<ForgotPasswordResponse> ForgotPassword(ForgotPasswordRequest forgotPasswordRequest)
        {
           var response = await _accountRepository.ForgotPasswordAsync(forgotPasswordRequest); 
            if(response.Status)
            {
                DtoUserBasicDetails userBasicDetails = new DtoUserBasicDetails()
                {
                    Email = forgotPasswordRequest.Email,
                };
                var userDetailsResponse = await _accountRepository.UserBasicDetailsGetAsync(userBasicDetails);
                if(userDetailsResponse.Status)
                {
                    string adminEmail = _configuration["AppSettings:AdminEmail"]!;

                    string body = Utilty.GetEmailTemplateBody("ForgotPasswordEmail.html");
                    body = body.Replace("{{username}}", userDetailsResponse.Data!.FirstName)
                              .Replace("{{link}}", response.Link)
                              .Replace("{{company-name}}", _configuration["AppSettings:CompanyName"])
                              .Replace("{{sendername}}", _configuration["AppSettings:CompanyName"]);


                    string mailSubject = EmailSubjectHelper.ForgotPasswordSubject.Replace("{{company-name}}", _configuration["AppSettings:CompanyName"]);
                    _emailService?.SendEmail(userDetailsResponse.Data.Email ?? "", mailSubject, body, true);
                }
                response.Status = true;
                response.Message = "Forgot password successful!";
            }
            return response;
        }

        public async Task<AuthResponse> ResetPassword(ResetPasswordRequest resetPassword)
        {
            var response = await _accountRepository.ResetPasswordAsync(resetPassword);
            if(response.Status)
            {
                DtoUserBasicDetails userBasicDetails = new DtoUserBasicDetails()
                {
                    Email = resetPassword.Email,
                };
                var userDetailsResponse = await _accountRepository.UserBasicDetailsGetAsync(userBasicDetails);
                if(userDetailsResponse.Status) 
                {
                    string adminEmail = _configuration["AppSettings:AdminEmail"]!;
                    string body = Utilty.GetEmailTemplateBody("ResetPasswordUpdatedEmail.html");

                    body = body.Replace("{{username}}", userDetailsResponse.Data!.FirstName)
                            .Replace("{{adminemail}}", adminEmail)
                            .Replace("{{company-name}}", _configuration["AppSettings:CompanyName"])
                            .Replace("{{sendername}}", _configuration["AppSettings:CompanyName"]);

                    string mailSubject = EmailSubjectHelper.ResetPasswordSubject.Replace("{{company-name}}", _configuration["AppSettings:CompanyName"]);
                    _emailService?.SendEmail(userDetailsResponse.Data.Email ?? "", mailSubject, body, true);
                }

                response.Status = true;
                response.Message = "Password Updated Successfully!";
            }
            return response;
            
        }

        public async Task<AuthResponse<ResendOtpResponse>> ResendOtp(ResendOtpRequest resendOtp)
        {
            AuthResponse<ResendOtpResponse> response = new();
            string? otp = await _authService.GenerateSecureOTP(6).ConfigureAwait(false);
            DtoSaveOtp dtoSaveOtp = new DtoSaveOtp()
            {
                Otp = otp,
                UserId = resendOtp.UserId,
                IsTwoFactorEnable = resendOtp.IsTwoFactorEnable,
            };
            var saveOtpResponse = await _accountRepository.SaveOtpAsync(dtoSaveOtp).ConfigureAwait(false);
            if(saveOtpResponse.Status)
            {

                DtoUserBasicDetails userBasicDetails = new DtoUserBasicDetails()
                {
                    UserId = resendOtp.UserId,
                };
                var userDetailsResponse = await _accountRepository.UserBasicDetailsGetAsync(userBasicDetails);
                if(userDetailsResponse.Status)
                {

                    string adminEmail = _configuration["AppSettings:AdminEmail"]!;
                    string body = Utilty.GetEmailTemplateBody("LoginOtpEmail.html");


                    body = body.Replace("{{otp}}", otp)
                        .Replace("{{username}}", userDetailsResponse.Data!.FirstName)
                        .Replace("{{sendername}}", _configuration["AppSettings:CompanyName"])
                        .Replace("{{adminemail}}", adminEmail)
                        .Replace("{{company-name}}", _configuration["AppSettings:CompanyName"]);

                    string mailSubject = EmailSubjectHelper.LoginOTPSubject.Replace("{{company-name}}", _configuration["AppSettings:CompanyName"]);
                    _emailService?.SendEmail(userDetailsResponse.Data.Email ?? "", mailSubject, body, true);

                    response.Status = true;
                    response.Message = "Otp sent successfully!";
                    response.Otp = otp;

                }
                else
                {
                    response.Message = Utilty.GetDataFromXML("ErrorMessage.xml", "error", userDetailsResponse.Message!);
                    response.Status = false;
                    response.Data = default;
                }           

            }
            else
            {
                response.Message = Utilty.GetDataFromXML("ErrorMessage.xml", "error", saveOtpResponse.Message!);
                response.Status = false;
                response.Data = default;
            }
            return response;
        }

        public async Task<AuthResponse> ChangePassword(ChangePasswordRequest changePassword )
        {
            var response = await _accountRepository.ChangePasswordAsync(changePassword);
            if(response.Status)
            {
                DtoUserBasicDetails userBasicDetails = new DtoUserBasicDetails()
                {
                    Email = changePassword.Email,
                };
                var userDetailsResponse = await _accountRepository.UserBasicDetailsGetAsync(userBasicDetails);
                if(userDetailsResponse.Status) 
                {
                    string adminEmail = _configuration["AppSettings:AdminEmail"]!;
                    string body = Utilty.GetEmailTemplateBody("PasswordChangeEmail.html");

                    body = body.Replace("{{username}}", userDetailsResponse.Data!.FirstName)
                              .Replace("{{adminemail}}", adminEmail)
                              .Replace("{{company-name}}", _configuration["AppSettings:CompanyName"])
                              .Replace("{{sendername}}", _configuration["AppSettings:CompanyName"]);

                    string mailSubject = EmailSubjectHelper.ChangePasswordSubject.Replace("{{company-name}}", _configuration["AppSettings:CompanyName"]);
                    _emailService?.SendEmail(userDetailsResponse.Data.Email ?? "", mailSubject, body, true);

                }
                response.Status = true;
                response.Message = "Password Updated Successfully!";

            }

            return response;
        }

        public async Task<ForgotPasswordResponse> VerifyPasswordToken(PasswordVerifyTokenRequest passwordVerifyTokenRequest)
        {
            var response = await _accountRepository.VerifyPasswordTokenAsync(passwordVerifyTokenRequest);
            return response;
        }

        public async Task<AuthResponse<UserBasicDetailsResponse>> VerifyOtp(DtoVerifyOtp dtoVerifyOtp)
        {
            var response = await _accountRepository.VerifyOtpAsync(dtoVerifyOtp);
            return response;
        }
    }
}