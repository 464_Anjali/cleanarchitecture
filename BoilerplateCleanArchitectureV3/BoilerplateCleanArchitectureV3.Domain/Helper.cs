﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Domain
{
    public static class EmailSubjectHelper
    {
        public static string LoginOTPSubject = "New User Login OTP | {{company-name}}";
        public static string UpdatePasswordOTPSubject = "Veriify OTP | {{company-name}}";
        public static string ResetPasswordSubject = "Reset Password Request | {{company-name}}";
        public static string AccountLockSubject = "User Account Lock | {{company-name}}";
        public static string UserCreateSubject = "New User Temporary Password | {{company-name}}";
        public static string ChangePasswordSubject = "Change Password Request | {{company-name}}";
        public static string ForgotPasswordSubject = "Forgot Password Request | {{company-name}}";
    }

    public static class MessageHelper
    {
        public static string NoMessage = "Message is not assigned";
    }

    public static class FlagHelper
    {
        public static string ResetPasswordScreen = "reset-password";
        public static string ChangePasswordScreen = "change-password";


    }
}
