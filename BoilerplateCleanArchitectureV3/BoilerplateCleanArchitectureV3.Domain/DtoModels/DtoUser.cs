﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Domain.DtoModels
{
    public class DtoUserDetailsGet
    {
        public string? UserId { get; set; } 
    }
}
