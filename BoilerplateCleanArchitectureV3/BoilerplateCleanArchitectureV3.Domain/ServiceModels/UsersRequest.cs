﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Domain.ServiceModels
{
    public class UsersAddRequest
    {
        [Required, StringLength(50, ErrorMessage = "The FirstName value cannot exceed 50 characters. "), RegularExpression("^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Invalid First Name")]
        public string? FirstName { get; set; }

        [Required, StringLength(50, ErrorMessage = "The FirstName value cannot exceed 50 characters. "), RegularExpression("^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Invalid Last Name")]
        public string? LastName { get; set; }

        [Required, EmailAddress, StringLength(150, ErrorMessage = "The Email cannot exceed 150 characters. ")]
        public string? Email { get; set; }

        [Required, StringLength(12, ErrorMessage = "The Phone Number value cannot exceed 12 characters. ")]
        public string? Mobile { get; set; }

        public string? OperationBy { get; set; } = "FDF9473F-C767-427C-9307-F1B285DDAADC";
    }

    public class UserUpdateRequest : UsersAddRequest
    {
        [Required]
        public string? UserId { get; set; }
    }

    public class UserDeleteRequest
    {
        [Required]
        public string? UserId { get; set; }
        public string? DeletedBy { get; set; } = "FDF9473F-C767-427C-9307-F1B285DDAADC";
    }

    public class UserDetailsResponse
    {
        public string? UserId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Mobile { get; set; }
    }
    public class UserListRequest
    {
        public string? SearchKey { get; set; }
        public int? Start { get; set; }
        public int? PageSize { get; set; }
        public string? SortCol { get; set; }
    }







}
