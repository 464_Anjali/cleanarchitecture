﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Domain.ServiceModels
{
    public class AccountCreationRequest
    {
        [Required, EmailAddress]
        public string? Email { get; set; }

        [Required, DataType(DataType.Password)]
        [RegularExpression(@"^(?!\S*\s)(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{6,15}$",
        ErrorMessage = "Password must be 6 to 15 Characters long without spaces and must contain at least one upper case character, one lowercase character, one number and one special character.")]

        public string? Password { get; set; }

        [Required]
        public string? RoleId { get; set; }

        [Required, StringLength(100, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "First name should not contain numbers and special characters")]
        public string? FirstName { get; set; }

        ////[StringLength(100, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 2)]
        //[RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "Middle name should not contain numbers and special characters")]

        //public string? MiddleName { get; set; }

        [Required, StringLength(100, ErrorMessage = "{0} must be at least {2} characters long.", MinimumLength = 2)]
        [RegularExpression(@"^[a-zA-Z\-\']+$", ErrorMessage = "Last name should not contain numbers and special characters")]
        public string? LastName { get; set; }

        [Required, DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\d{3}-\d{3}-\d{4}$", ErrorMessage = "Not a valid phone number. Phone Number must have a format as 000-000-0000.")]

        public string? Mobile { get; set; }

        // [Required]
        // public string? Address1 { get; set; }

        // public string? Address2 { get; set; }

        //// [Required]
        // public string? State { get; set;}

        // //[Required]
        // public string? City { get; set; }

        //// [Required]
        // public string? ZipCode { get; set; }

        [Required]
        public Guid? CreatedBy { get; set; }


    }


    public class UserBasicDetailsResponse
    {
        public string? UserId { get; set; }
        public string? Email { get; set; }
        public string? FirstName { get; set; }
        // public string? MiddleName { get; set; }
        public string? LastName { get; set; }

        public string? Mobile { get; set; }
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }
        public string? Role { get; set; }
      
        //public string? Address1 { get; set; }
        //public string? Address2 { get; set; }
        //public string? State { get; set; }
        //public string? City { get; set; }
        //public string? ZipCode { get; set; }
        public string? CreatedBy { get; set; }
        public bool IsTwoFactorEnable { get; set; }


    }

    public class DtoUserBasicDetails
    {
        public string? UserId { get; set; }
        public string? Email { get; set; }
    }

    /// <summary>
    /// User Account Login
    /// </summary>
    public class DtoLogin
    {
        [Required, EmailAddress]
        public string? Email { get; set; }

        [Required, DataType(DataType.Password)]
        [RegularExpression(@"^(?!\S*\s)(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{6,15}$",
        ErrorMessage = "Password must be 6 to 15 Characters long without spaces and must contain at least one upper case character, one lowercase character, one number and one special character.")]
        public string? Password { get; set; }
    }

    public class ForgotPasswordRequest
    {
        [Required]
        public string? Email { get; set; }
    }

    public class PasswordVerifyTokenRequest
    {
        public string? Email { get; set; }
        public string? EmailToken { get; set; }

    }

    public class ResetPasswordRequest
    {
        public string? Email { get; set; }
        // public string? EmailToken { get; set; }
        //public Guid? UserId { get; set; }
        public string? Password { get; set; }
        public byte[]? PasswordHash { get; set; }
        public byte[]? PasswordSalt { get; set; }
        public Guid? UpdatedBy { get; set; }
        //public string? Opr { get; set; }
        //public bool IsEncrypted { get; set; }
        //public string? ScreenName { get; set; }       

    }

    public class ResendOtpRequest
    {
        public string? UserId { get; set; }
        public bool IsTwoFactorEnable { get; set; }
    }
    public class ResendOtpResponse
    {
        public string? UserId { get; set; }
        public bool? IsTwoFactorEnable { get; set; }
    }

    public class ChangePasswordRequest
    {
        [Required]
        public string? Email { get; set; }

        //public string? EmailToken { get; set; }
        [Required]
        public string? OldPassword { get; set; }

        [Required, DataType(DataType.Password)]
        [RegularExpression(@"^(?!\S*\s)(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{6,15}$",
        ErrorMessage = "Password must be 6 to 15 Characters long without spaces and must contain at least one upper case character, one lowercase character, one number and one special character.")]
        public string? NewPassword { get; set; }

        [DataType(DataType.Password), Compare("NewPassword")]
        public string? ConfirmPassword { get; set; }
        public string? UpdatedBy { get; set; }
        //public string? ScreenName { get; set; }
        //public string? Opr { get; set; }

    }
}
