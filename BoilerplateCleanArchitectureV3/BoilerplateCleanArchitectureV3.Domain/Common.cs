﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoilerplateCleanArchitectureV3.Domain
{
    public class DataTableParams
    {
        public string? SearchKey { get; set; }

        [Required]
        public int Start { get; set; }

        [Required]
        public int PageSize { get; set; }

        public string? SortCol { get; set; }
        public string? Draw { get; set; }
    }
    public class Appsettings
    {
        public string? Secret { get; set; }
        public Smtp? Smtp { get; set; }
    }

    public class Smtp
    {
        public string? Host { get; set; }
        public string? Port { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }

        //public bool enableSSl { get; set; }
        public string? EnableSSl { get; set; }

        public string? From { get; set; }
    }


    public class ResponseObj
    {
        public bool? Status { get; set; }
        public string? Message { get; set; }

        public string? FileDownloadName { get; set; }
    }

    public class DownloadDataParam
    {
        public string? FileType { get; set; }
        public string? Module { get; set; }
    }

    public class ResponseListObject
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public object? data { get; set; }
        public long? totalRecords { get; set; } = 0;
    }
}
