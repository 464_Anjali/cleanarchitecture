﻿using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Reflection;

namespace CommonComponent.Email
{
    public class EmailService : IEmailService
    {
        private static EmailConfig? _emailConfig;
        private static string? _connectionString;

        public EmailService(EmailConfig emailConfig, string connectionString)
        {
            _emailConfig = emailConfig;
            _connectionString = connectionString;
        }

        private static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }

        public async Task<string> GetEmailContent(string templateName, object replaceParametersObject)
        {
            var content = await GetEmailContent(templateName);
            Type tModelType = replaceParametersObject.GetType();

            //We will be defining a PropertyInfo Object which contains details about the class property
            PropertyInfo[] arrayPropertyInfos = tModelType.GetProperties();
            foreach (var item in arrayPropertyInfos)
            {
                content = content.Replace("{{" + item.Name.ToUpper() + "}}", item.GetValue(replaceParametersObject)?.ToString());
            }

            return content;
        }

        private static async Task<string> GetEmailContent(string TemplateName)
        {
            using var connections = Connection;
            var response = (await connections.ExecuteScalarAsync<string>(_emailConfig?.EmailContentSpName, new
            {
                TemplateName
            }, commandType: CommandType.StoredProcedure)).ToString(CultureInfo.CurrentCulture);

            return response;
        }

        #region Send Email

        public async Task<CommonComponentResponse> SendEmail(string recipient, string subject, string body, bool isHtml)
        {
            CommonComponentResponse response = new();

            if (_emailConfig == null)
            {
                response.Message = "Email config is not provided.";
                return response;
            }

            SmtpClient client = new(_emailConfig.Host, _emailConfig.Port);
            MailMessage mailMessage = new();
            try
            {
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(_emailConfig.Username, _emailConfig.Password);
                client.EnableSsl = true;
                mailMessage.From = new MailAddress(_emailConfig.FromEmail);
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = isHtml;
                mailMessage.Subject = subject;
                mailMessage.To.Add(new MailAddress(recipient));

                await client.SendMailAsync(mailMessage).ConfigureAwait(false);
                response.Status = true;
            }
            catch (SmtpFailedRecipientsException ex)
            {
                response.Status = false;
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        System.Threading.Thread.Sleep(50000);
                        client.Send(mailMessage);
                    }
                    else
                    {
                        response.Message = "SmtpFailedRecipientsException: " + ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = "Error Occured while sending email: " + ex.Message;
            }
            return response;
        }

        public async Task<CommonComponentResponse> SendEmail(List<string> recipients, string subject, string body, bool isHtml)
        {
            CommonComponentResponse response = new();

            foreach (var item in recipients)
            {
                response = await SendEmail(item, subject, body, isHtml);
            }

            return response;
        }

        #endregion Send Email
    }
}
