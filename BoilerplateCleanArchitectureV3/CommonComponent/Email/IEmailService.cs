﻿namespace CommonComponent.Email
{
    public interface IEmailService
    {
        Task<string> GetEmailContent(string templateName, object replaceParametersObject);

        Task<CommonComponentResponse> SendEmail(string recipient, string subject, string body, bool isHtml);

        Task<CommonComponentResponse> SendEmail(List<string> recipients, string subject, string body, bool isHtml);
    }
}