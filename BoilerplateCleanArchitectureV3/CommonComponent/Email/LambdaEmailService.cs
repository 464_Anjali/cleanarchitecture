﻿using CommonComponent.Lambda;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Net.Mail;
using System.Reflection;
using Dapper;

namespace CommonComponent.Email
{
    public class LambdaEmailService : IEmailService
    {
        private static string? _emailContentSpName;
        private static string? _connectionString;
        private readonly ILambdaService _scheduleLambda;

        public LambdaEmailService(string emailContentSpName, string connectionString, ILambdaService scheduleLambda)
        {
            _emailContentSpName = emailContentSpName;
            _connectionString = connectionString;
            _scheduleLambda = scheduleLambda;
        }

        private static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(_connectionString);
            }
        }

        public async Task<string> GetEmailContent(string templateName, object replaceParametersObject)
        {
            var content = await GetEmailContent(templateName);
            Type tModelType = replaceParametersObject.GetType();

            //We will be defining a PropertyInfo Object which contains details about the class property
            PropertyInfo[] arrayPropertyInfos = tModelType.GetProperties();
            foreach (var item in arrayPropertyInfos)
            {
                content = content.Replace("{{" + item.Name.ToUpper() + "}}", item.GetValue(replaceParametersObject)?.ToString());
            }

            return content;
        }

        private static async Task<string> GetEmailContent(string TemplateName)
        {
            using var connections = Connection;
            var response = (await connections.ExecuteScalarAsync<string>(_emailContentSpName, new
            {
                TemplateName
            }, commandType: CommandType.StoredProcedure)).ToString(CultureInfo.CurrentCulture);

            return response;
        }

        #region Send Email

        public async Task<CommonComponentResponse> SendEmail(string recipient, string subject, string body, bool isHtml)
        {
            CommonComponentResponse response = new();
            try
            {
                await _scheduleLambda.InvokeEmailAsync(new EmailRequest
                {
                    To = recipient,
                    Subject = subject,
                    Body = body
                });
                response.Status = true;
            }
            catch (SmtpFailedRecipientsException ex)
            {
                response.Status = false;
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy ||
                        status == SmtpStatusCode.MailboxUnavailable)
                    {
                        Thread.Sleep(50000);
                        await _scheduleLambda.InvokeEmailAsync(new EmailRequest
                        {
                            To = recipient,
                            Subject = subject,
                            Body = body
                        });
                    }
                    else
                    {
                        response.Message = "SmtpFailedRecipientsException: " + ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = "Error Occured while sending email: " + ex.Message;
            }
            return response;
        }

        public async Task<CommonComponentResponse> SendEmail(List<string> recipients, string subject, string body, bool isHtml)
        {
            CommonComponentResponse response = new();

            foreach (var item in recipients)
            {
                response = await SendEmail(item, subject, body, isHtml);
            }

            return response;
        }

        #endregion Send Email
    }
}
