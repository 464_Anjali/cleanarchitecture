﻿
namespace CommonComponent
{
    public class CommonComponentResponse
    {
        public bool Status { get; set; }

        public string? Message { get; set; }
    }

    public class CommonComponentResponse<T> : CommonComponentResponse
    {
        public T? Data { get; set; }
    }

    public class CommonComponentResponseList<T> : CommonComponentResponse
    {
        public List<T>? Data { get; set; }
    }

    public class AuthPagedResponseList<T> : CommonComponentResponse
    {
        public List<T>? Data { get; set; }

        public int RecordsTotal { get; set; }

        public string? Draw { get; set; }
    }
}
