﻿namespace CommonComponent.Message
{
    public class MessageConfig
    {
        public string? AccountSID { get; set; }

        public string? TwilioAuthToken { get; set; }

        public string? FromNumber { get; set; }

        public string? SMSContentSpName { get; set; }
    }
}