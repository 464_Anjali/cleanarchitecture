﻿using Newtonsoft.Json;

namespace CommonComponent.Lambda
{
    public class LambdaRequestPayload
    {
        [JsonProperty("dueDate")]
        public DateTime? DueDate { get; set; }

        [JsonProperty("base_url")]
        public string? BaseURL { get; set; }

        [JsonProperty("path")]
        public string? Path { get; set; }

        [JsonProperty("arn")]
        public string? ARNId { get; set; }

        [JsonProperty("emailRequest")]
        public EmailRequest? EmailRequest { get; set; }

        [JsonProperty("notificationRequest")]
        public List<NotificationRequest>? NotificationRequest { get; set; }

        [JsonProperty("webSocketRequest")]
        public WebsocketRequest? WebsocketRequest { get; set; }

        [JsonProperty("databaseRequest")]
        public DatabaseRequest? DatabaseRequest { get; set; }
    }

    public class EmailRequest
    {
        [JsonProperty("to")]
        public string? To { get; set; }

        [JsonProperty("subject")]
        public string? Subject { get; set; }

        [JsonProperty("body")]
        public string? Body { get; set; }
    }

    public class NotificationRequest
    {
        [JsonProperty("deviceToken")]
        public IEnumerable<string>? DeviceToken { get; set; }
        public IEnumerable<string>? SendToUsers { get; set; }
        public string Title { get; set; } = "";
        public string Body { get; set; } = "";
        public bool IsImportant { get; set; }
        public string Type { get; set; } = "";

        [JsonProperty("data")]
        public object? Data { get; set; }
        [JsonProperty("notification")]
        public object? Notification { get; set; }
    }

    public class WebsocketRequest
    {
        public string? SignalRType { get; set; }

        public string? ConnectionId { get; set; }

        public string? Title { get; set; }

        public string? Body { get; set; }

        public string? Type { get; set; }

        public object? Data { get; set; }
    }

    public class DatabaseRequest
    {
        public string? NotificationId { get; set; }
        public string? ToUserId { get; set; }
        public string? FromUserId { get; set; }
        public string? Title { get; set; }
        public string? Body { get; set; }
        public string? DeviceToken { get; set; }
        public string? ExtraDataJson { get; set; }
        public string? Type { get; set; }
    }
}