﻿namespace CommonComponent.Lambda
{
    public interface ILambdaService
    {
        Task InvokeEmailAsync(EmailRequest request);

        Task InvokeNotificationAsync(NotificationRequest request);

        Task InvokeSendReminder(LambdaRequestPayload request);

        Task<string> InvokeSchedulerAsync(LambdaRequestPayload lambdaRequest);

        Task CancelInvoke(string arn);
    }
}