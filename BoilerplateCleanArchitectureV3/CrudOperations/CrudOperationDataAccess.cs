﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace CrudOperations
{
    public class CrudOperationDataAccess : ICrudOperationService
    {
        private readonly IConfiguration _configuration; // Configuration for accessing application settings
        private readonly string _connectionString; // Database connection string

        // Constructor to initialize configuration and connection string
        public CrudOperationDataAccess(IConfiguration configuration, string connectionString)
        {
            _configuration = configuration;
            _connectionString = connectionString;
        }

        // Property to get a new instance of IDbConnection based on the connection string
        public IDbConnection Connection => new SqlConnection(_connectionString);

        // Method to retrieve a connection string by name from configuration
        public string GetConnectionString(string connectionName) => _configuration.GetConnectionString(connectionName)!;

        // Method to retrieve a configuration section by key
        public IConfigurationSection GetConfigurationSection(string key) => _configuration.GetSection(key);

        // Method to retrieve a configuration value based on a specific node and key using string interpolation
        public string AppSettingsKeys(string nodeName, string key) => _configuration[$"{nodeName}:{key}"]!;


        public Task<T> Insert<T>(string storedProcedureName, object parameters)
        {
            T response;
            using var connections = Connection;
            try
            {
                response = connections.QueryFirstOrDefault<T>(
                    sql: storedProcedureName,
                    param: parameters,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                    )!;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
            return Task.FromResult(response);
        }

        public async Task<Response<T>> InsertAndGet<T>(string storedProcedureName, object parametersPocoObj)
        {
            using var connections = Connection;
            try
            {
                var result = await connections.QueryMultipleAsync(
                    sql: storedProcedureName,
                    param: parametersPocoObj,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                    );

                var response = result.Read<Response<T>>().FirstOrDefault();
                response!.Data = response.Status ? result.Read<T>().FirstOrDefault() : default;
                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }

        public Task<T> Update<T>(string storedProcedureName, object parameters)
        {
            T response;
            using var connections = Connection;
            try
            {
                response = connections.QueryFirstOrDefault<T>(
                    sql: storedProcedureName,
                    param: parameters,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                    )!;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
            return Task.FromResult(response);
        }

        public Task<T> Delete<T>(string storedProcedureName, object parameters)
        {
            T response;
            using (var connections = Connection)
            {
                try
                {
                    response = connections.QueryFirstOrDefault<T>(
                        sql: storedProcedureName,
                        param: parameters,
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure
                        )!;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (connections.State == ConnectionState.Open)
                        connections.Close();
                }
                return Task.FromResult(response);
            }
        }

        public Task<T> InsertUpdateDelete<T>(string storedProcedureName, object spParamsPocoMapper)
        {
            T response;
            using var connections = Connection;
            try
            {
                response = connections.QueryFirstOrDefault<T>(
                    sql: storedProcedureName,
                    param: spParamsPocoMapper,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                    )!;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
            return Task.FromResult(response);
        }


        public async Task<Response<T>> GetSingleRecord<T>(string storedProcedureName, object parameters)
        {
            using (var connections = Connection)
            {
                try
                {
                    var result = await connections.QueryMultipleAsync(
                        sql: storedProcedureName,
                        param: parameters,
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure
                        );

                    Response<T> response = result.Read<Response<T>>().FirstOrDefault()!;
                    response.Data = response.Status ? result.Read<T>().FirstOrDefault() : default;
                    return response;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (connections.State == ConnectionState.Open)
                        connections.Close();
                }
            }
        }

        public async Task<Response<Tuple<T1, List<T2>>>> GetSingleRecord<T1, T2>(string storedProcedureName, object parameters)
        {
            using (var connections = Connection)
            {
                try
                {
                    var result = await connections.QueryMultipleAsync(
                        sql: storedProcedureName,
                        param: parameters,
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure
                        );

                    Response<Tuple<T1, List<T2>>> response = result.Read<Response<Tuple<T1, List<T2>>>>().FirstOrDefault()!;
                    var firstObject = response.Status ? result.Read<T1>().FirstOrDefault() : default;
                    var listObject = response.Status ? result.Read<T2>()?.ToList() : default;
                    response.Data = new Tuple<T1, List<T2>>(firstObject!, listObject!);

                    return response;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (connections.State == ConnectionState.Open)
                        connections.Close();
                }
            }

        }

        public async Task<ResponseList<T>> GetList<T>(string storedProcedureName, object parameters)
        {
            using var connections = Connection;
            try
            {
                var result = await connections.QueryMultipleAsync(
                    sql: storedProcedureName,
                    param: parameters,
                    commandTimeout: null,
                    commandType: CommandType.StoredProcedure
                    );

                ResponseList<T> response = result.Read<ResponseList<T>>().FirstOrDefault()!;
                response.Data = response.Status ? result.Read<T>().ToList() : default;
                return response;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connections.State == ConnectionState.Open)
                    connections.Close();
            }
        }

        public async Task<ResponseList<T>> GetPaginatedList<T>(string storedProcedureName, object parameters)
        {
            using (var connections = Connection)
            {
                try
                {
                    var result = await connections.QueryMultipleAsync(
                        sql: storedProcedureName,
                        param: parameters,
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure
                        );

                    ResponseList<T> response = result.Read<ResponseList<T>>().FirstOrDefault()!;
                    response.Data = response.Status ? result.Read<T>().ToList() : default;
                    response.TotalRecords = response.RecordsFiltered = response.Status ? result.Read<int>().SingleOrDefault() : 0;
                    return response;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (connections.State == ConnectionState.Open)
                        connections.Close();
                }
            }
        }

        public async Task<ResponseListCopy<T>> GetPaginatedListCopy<T>(string storedProcedureName, object parameters)
        {
            using (var connections = Connection)
            {
                try
                {
                    var result = await connections.QueryMultipleAsync(
                        sql: storedProcedureName,
                        param: parameters,
                        commandTimeout: null,
                        commandType: CommandType.StoredProcedure
                        );

                    ResponseListCopy<T> response = result.Read<ResponseListCopy<T>>().FirstOrDefault()!;
                    response.Data = response.Status ? result.Read<T>().ToList() : default;
                    //response.Description = result.Read<string>().SingleOrDefault();
                    response.Amount = result.Read<string>().SingleOrDefault();
                    response.InvoiceNumber = result.Read<int>().SingleOrDefault();
                    response.DateOfIssue = result.Read<string>().SingleOrDefault();

                    // response.TotalRecords = response.RecordsFiltered = response.Status ? result.Read<int>().SingleOrDefault() : 0;
                    return response;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (connections.State == ConnectionState.Open)
                        connections.Close();
                }
            }
        }

        public async Task<Response<T>?> ExecuteStoredProcAsync<T>(string storedProcedureName, object parameters, string connectionType)
        {
            using var connection = Connection;

            try
            {
                switch (connectionType.ToLower())
                {
                    case "queryfirstordefaultasync":
                        return await connection.QueryFirstOrDefaultAsync<Response<T>>(
                            sql: storedProcedureName,
                            param: parameters,
                            commandTimeout: null,
                            commandType: CommandType.StoredProcedure
                        ).ConfigureAwait(false);

                    case "querymultipleasync":
                        var result = await connection.QueryMultipleAsync(
                            sql: storedProcedureName,
                            param: parameters,
                            commandTimeout: null,
                            commandType: CommandType.StoredProcedure
                        ).ConfigureAwait(false);

                        var response = result.Read<Response<T>>().FirstOrDefault();
                        response!.Data = response.Status ? result.Read<T>().FirstOrDefault() : default;
                        return response;

                    case "queryfirstordefault":
                        return connection.QueryFirstOrDefault<Response<T>>(
                            sql: storedProcedureName,
                            param: parameters,
                            commandTimeout: null,
                            commandType: CommandType.StoredProcedure
                        )!;

                    default:
                        throw new ArgumentException("Invalid Connection Type");
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }


    }
}


    
